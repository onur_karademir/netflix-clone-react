import React, { useState, createContext, useContext } from "react";

import {
  Container,
  Inner,
  Header,
  Title,
  Body,
  Item,
} from "./Accordion.elements";

const ToggleContext = createContext();

const Accordion = ({ children, ...restProps }) => {
  return (
    <Container {...restProps}>
      <Inner>{children}</Inner>
    </Container>
  );
};

Accordion.Container = function AccordionContainer({ children, ...restProps }) {
  return <Container {...restProps}>{children}</Container>;
};
Accordion.Inner = function AccordionInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Accordion.Item = function AccordionItem({ children, ...restProps }) {
  const [toggle, setToggle] = useState(false);
  return (
    <ToggleContext.Provider value={{ toggle, setToggle }}>
      <Item {...restProps}>{children}</Item>;
    </ToggleContext.Provider>
  );
};
Accordion.Title = function AccordionTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
Accordion.Header = function AccordionHeader({ children, ...restProps }) {
    const {toggle,setToggle} = useContext(ToggleContext);
  return <Header onClick={()=> setToggle(!toggle)} {...restProps}>
      {children}
      {toggle ? (
        <img src="/images/icons/close-slim.png" alt="Close" />
      ) : (
        <img src="/images/icons/add.png" alt="Open" />
      )}
      </Header>;
};
Accordion.Body = function AccordionBody({ children, ...restProps }) {
    const {toggle} = useContext(ToggleContext);
    
  return toggle ? <Body {...restProps}>{children}</Body> : null;
};

export default Accordion;
