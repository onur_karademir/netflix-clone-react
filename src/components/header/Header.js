import React from 'react'
import {Link as ReactRouterLink} from "react-router-dom";
import {Backgrond, Container, Logo, ButtonLink} from "./Header.elements";

const Header = ({bg = 'true', children, ...restProps}) => {
return bg ? <Backgrond {...restProps}>{children}</Backgrond> : children;
}

Header.Container = function HeaderContainer ({children,...restProps}) {
return<Container {...restProps}>{children}</Container>
}
Header.Logo = function HeaderLogo ({to,...restProps}) {
return <ReactRouterLink to={to}>
    <Logo {...restProps}></Logo>
</ReactRouterLink>  
}
Header.ButtonLink = function HeaderButtonLink ({children,...restProps}) {
return<ButtonLink {...restProps}>{children}</ButtonLink>
}



export default Header
