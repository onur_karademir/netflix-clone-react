import React from 'react'
import {Container,Top,Description,Input,Button,FormContainer} from "./HeaderText.elements";

const HeaderText = ({children,...restProps}) => {
    return (
    <Container {...restProps}>{children}</Container>
    )
}
HeaderText.Container = function HeaderTextContainer ({children,...restProps}) {
    return<Container {...restProps}>{children}</Container>
}
HeaderText.Top = function HeaderTextTop ({children,...restProps}) {
    return<Top {...restProps}>{children}</Top>
}
HeaderText.Description = function HeaderTextDescription ({children,...restProps}) {
    return<Description {...restProps}>{children}</Description>
}
HeaderText.Input = function HeaderTextInput ({children,...restProps}) {
    return<Input {...restProps} />
}
HeaderText.Button = function HeaderTextButton ({children,...restProps}) {
    return<Button {...restProps}>{children}</Button>
}
HeaderText.FormContainer = function HeaderTextFormContainer ({children,...restProps}) {
    return<FormContainer {...restProps}>{children}</FormContainer>
}
export default HeaderText
