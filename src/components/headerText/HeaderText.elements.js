import styled from "styled-components/macro"; 

export const Container = styled.div `
display:flex;
flex-direction:column;
text-align:center;
color:white;
text-transform:uppercase;
padding:50px 5%;
@media screen and (max-width:600px) {
    padding:35px 2%;
}
`
export const Top = styled.h1 `
font-size:35px;
text-align:center;
color:white;
text-transform:uppercase;
line-height:1.1;
margin-bottom:2rem;
@media (max-width:600px) {
  font-size:25px;
  margin-bottom:1rem;
}
`
export const Description = styled.h3`
font-size:20px;
color:white;
text-align:center;
line-height:1.1;
margin-bottom:2rem;
@media (max-width:600px) {
  font-size:15px;
  margin-bottom:1rem;
}
`
export const FormContainer = styled.div`
display:flex;
flex-direction:row;
flex-wrap:wrap;
justify-content:center;
align-items:center;
width: 100%;
@media screen and (max-width:600px){
    flex-direction:column;
}
`
export const Input = styled.input`
display:flex;
flex-wrap:wrap;
padding:10px;
border:0;
width: 450px;
height: 70px;
`
export const Button = styled.button`
height: 70px;
padding:0 30px;
text-transform:uppercase;
align-items:center;
border:none;
outline:0;
color:white;
background:red;
@media (max-width:600px) {
  width: 80%;
  margin-top:3%;
}
`