import React from "react";
import { Container, Input, Text, Button } from "./OptForm.elements";

const OptForm = ({ children, ...restProps }) => {
  return <Container {...restProps}>{children}</Container>;
}

OptForm.Input = function OptFormInput({ ...restProps }) {
    return <Input {...restProps} />;
};
OptForm.Text = function OptFormText({ children, ...restProps }) {
return <Text {...restProps}>{children}</Text>;
};
OptForm.Button = function OptFormButton({ children, ...restProps }) {
  return <Button {...restProps }>{children}<img src="/images/icons/chevron-right.png" alt="Try Now" /></Button>;
};

export default OptForm;
