import styled from 'styled-components/macro';

export const Item = styled.div `
display:flex;
border-bottom: 2px solid #222;
padding:50px 5%;
color:white;
owerflow:hidden;
`

export const Inner = styled.div `
display:flex;
justify-content:space-between;
align-items:center;
max-width:1100px;
margin:auto;
width: 100%;
flex-direction:${({ direction}) => direction };

@media screen and (max-width:1000px) {
    flex-direction:column;
}
`
export const Order = styled.div `
width:50%;
@media screen and (max-width:1000px) {
 width:100%;
 text-align:center;
 padding:0 45px;
}
`
export const Title = styled.h1 `
font-size:50px;
font-weight:normal;
line-height:1.1;
margin-bottom:8px;
@media screen and (max-width:600px) {
    font-size:36px;
}

`
export const SubTitle = styled.h2 `
font-size:26px;
line-height:normal;
font-weight:normal;
@media screen and (max-width:600px) {
    font-size:18px;
}

`
export const Image = styled.img `
max-width: 100%;
height: auto;
`
export const Container = styled.div `
@media (max-width:1000px) {
    ${Item}:last-of-type h2{
        margin-bottom:50px
    }
}
`