import React, { useState, useContext } from "react";
import { useHistory } from 'react-router-dom';
import { FirebaseContext } from "../context/firebase";
import { Form } from "../components";
import FooterContainer from "../containers/FooterContainer";
import HeaderContainer from "../containers/HeaderContainer";
import * as ROUTES from '../constants/Routes';

export default function Signin() {
    const history = useHistory();
  const { firebase } = useContext(FirebaseContext);

  const [firstName, setFirstName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");

  const isInvalid = firstName === ''|| password === "" || email === "";

  const signUpHendler = (e) => {
    e.preventDefault();

    // firebase work//
    return firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
       result.user
       .updateProfile ({
           displayName:firstName,
           photoUrl: Math.floor(Math.random * 5) + 1,
       }).then(()=> {
           history.push(ROUTES.BROWSE);
       })
      })
      .catch((error) => {
        setEmail('');
        setPassword('');
        setError(error.message);
      });
  };

  return (
    <>
      <HeaderContainer>
        <Form>
          <Form.Title>Sign up</Form.Title>
          {error && <Form.Error>{error}</Form.Error>}
          <Form.FormBase onSubmit={signUpHendler} method="POST">
            <Form.Input
              onChange={({ target }) => setFirstName(target.value)}
              value={firstName}
              type="text"
              placeholder="First Name"
            ></Form.Input>
            <Form.Input
              onChange={({ target }) => setEmail(target.value)}
              value={email}
              type="text"
              placeholder="Email Adress"
            ></Form.Input>
            <Form.Input
              onChange={({ target }) => setPassword(target.value)}
              value={password}
              type="password"
              placeholder="password"
              autoComplate="off"
            ></Form.Input>
            <Form.Submit disabled={isInvalid}>Sign up</Form.Submit>
          </Form.FormBase>

          <Form.Text>
            New to Netflix? <Form.Link to="/signin">Sign in now.</Form.Link>
          </Form.Text>
          <Form.TextSmall>
            This page is protected by Google reCAPTCHA to ensure you're not a
            bot. Learn more.
          </Form.TextSmall>
        </Form>
      </HeaderContainer>
      <FooterContainer></FooterContainer>
    </>
  );
}
