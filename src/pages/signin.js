import React, { useState, useContext } from "react";
import { useHistory } from 'react-router-dom';
import { FirebaseContext } from "../context/firebase";
import { Form } from "../components";
import FooterContainer from "../containers/FooterContainer";
import HeaderContainer from "../containers/HeaderContainer";
import * as ROUTES from '../constants/Routes';

export default function Signin() {
    const history = useHistory();
  const { firebase } = useContext(FirebaseContext);

  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [error, setError] = useState("");

  const isInvalid = password === "" || email === "";

  const submitHendler = (e) => {
    e.preventDefault();

    // firebase work//
    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        history.push(ROUTES.BROWSE);
      })
      .catch((error) => {
        setEmail('');
        setPassword('');
        setError(error.message);
      });
  };

  return (
    <>
      <HeaderContainer>
        <Form>
          <Form.Title>Sign in</Form.Title>
          {error && <Form.Error>{error}</Form.Error>}
          <Form.FormBase onSubmit={submitHendler} method="POST">
            <Form.Input
              onChange={({ target }) => setEmail(target.value)}
              value={email}
              type="text"
              placeholder="Email Adress"
            ></Form.Input>
            <Form.Input
              onChange={({ target }) => setPassword(target.value)}
              value={password}
              type="password"
              placeholder="password"
              autoComplate="off"
            ></Form.Input>
            <Form.Submit disabled={isInvalid}>Sign in</Form.Submit>
          </Form.FormBase>

          <Form.Text>
            New to Netflix? <Form.Link to="/signup">Sign up now.</Form.Link>
          </Form.Text>
          <Form.TextSmall>
            This page is protected by Google reCAPTCHA to ensure you're not a
            bot. Learn more.
          </Form.TextSmall>
        </Form>
      </HeaderContainer>
      <FooterContainer></FooterContainer>
    </>
  );
}
