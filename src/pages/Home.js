import React from "react";

import HeaderContainer from "../containers/HeaderContainer";
import FaqsDataAccordion from "../containers/FaqsDataAcordion";
import FooterContainer from "../containers/FooterContainer";
import JumbotronContainer from "../containers/JumbotronContainer";
const Home = () => {
  return (
    <>
      <HeaderContainer>
      </HeaderContainer>
      <JumbotronContainer />
      <FaqsDataAccordion />
      <FooterContainer />
    </>
  );
};

export default Home;
