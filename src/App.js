import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import * as ROUTES from "./constants/Routes";
import {Home, Browser, Signin, Signup} from "./pages";
// import { v4 as uuidv4 } from 'uuid'; /// uuid import exemple //

//if you want use axios -->>> import_npm_list.txt inside axios you can learn from web page//

function App() {
  return (
    <Router>
      <Route exact path="/signin">
        <Signin />
      </Route>
      <Route exact path="/signup">
      <Signup />
      </Route>
      <Route exact path="/browse">
        <Browser />
      </Route>

      <Route exact path={ROUTES.HOME}>
       <Home></Home>
      </Route>
    </Router>
  );
}

export default App;
