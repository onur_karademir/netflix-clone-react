import React from 'react'
import {Accordion, OptForm} from "../components"
import faqsData from "../fixtures/faqs.json";

const FaqsDataAcordion = () => {
    return (
        <Accordion>
            <Accordion.Title>Titile</Accordion.Title>
            {faqsData.map((item)=> (
                <Accordion.Item key={item.key}>
                  <Accordion.Header>{item.header}</Accordion.Header>
                <Accordion.Body>{item.body}</Accordion.Body>
                </Accordion.Item>
                
            ))}
            <OptForm>
                <OptForm.Input placeholder="Email address"></OptForm.Input>
                <OptForm.Button>Try it now</OptForm.Button>
                <OptForm.Text>Ready to watch? Enter your email to create or restart your membership.</OptForm.Text>
            </OptForm>
        </Accordion>
    )
}

export default FaqsDataAcordion

