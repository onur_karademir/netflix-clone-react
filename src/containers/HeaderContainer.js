import React from "react";
import { Header, HeaderText } from "../components";
import * as ROUTES from "../constants/Routes";
import Logo from "../logo.svg";
const HeaderContainer = ({children, ...restProps}) => {
  return (
    <Header>
      <Header.Container>
        <Header.Logo to={ROUTES.HOME} alt="Netflix" src={Logo} />
        <Header.ButtonLink to={ROUTES.SIGN_IN}>Sign in</Header.ButtonLink>
      </Header.Container>
      <HeaderText>
        <HeaderText.Top>
          Sınırsız film, dizi ve çok daha fazlası.
        </HeaderText.Top>
        <HeaderText.Description>
          İstediğiniz yerde izleyin. İstediğiniz zaman iptal edin.
        </HeaderText.Description>
        <HeaderText.FormContainer>
        <HeaderText.Input type="text" placeholder="Email"/>
        <HeaderText.Button>Try it now</HeaderText.Button>
        </HeaderText.FormContainer>
      </HeaderText>
      { children }
    </Header>
  );
};

export default HeaderContainer;
