import React from "react";
import Jumbotron from "../components/jumbotron/Jumbotron";
import jumboData from "../fixtures/jumboData.json";
// import { v4 as uuidv4 } from 'uuid'; /// uuid import exemple //

//if you want use axios -->>> import_npm_list.txt inside axios you can learn from web page//

function JumbotronContainer() {
  return (
    <Jumbotron.Container>
      {jumboData.map((item) => (
        <Jumbotron key={item.id} direction={item.direction}>
          <Jumbotron.Order>
            <Jumbotron.Title>{item.title}</Jumbotron.Title>
            <Jumbotron.SubTitle>{item.subTitle}</Jumbotron.SubTitle>
          </Jumbotron.Order>
          <Jumbotron.Order>
            <Jumbotron.Image src={item.image} alt={item.alt}></Jumbotron.Image>
          </Jumbotron.Order>
        </Jumbotron>
      ))}
    </Jumbotron.Container>
  );
}

export default JumbotronContainer;