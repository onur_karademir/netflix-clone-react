import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import { firebase } from './lib/firebase.prod';
import { FirebaseContext } from './context/firebase';
ReactDOM.render(
  <React.StrictMode>
    <FirebaseContext.Provider value={{ firebase }}>
      <App />
    </FirebaseContext.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

